import 'package:flutter/material.dart';
import 'package:pokedex_app/page/pokemon_page.dart';
import 'package:pokedex_app/widget/custom_button.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('L\'application des cartes pokemon'),
      ),
      body: ListView(
        padding: const EdgeInsets.symmetric(
          horizontal: 32.0,
          vertical: 16.0,
        ),
        children: [
          CustomButton('Des pokemons', () {
            Navigator.of(context).push(MaterialPageRoute(builder: (ctx) {
              return PokemonPage();
            }));
          }),
        ],
      ),
    );
  }
}
