import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pokedex_app/api/pokemon_api.dart';

class PokemonPage extends StatefulWidget {
  @override
  _PokemonPageState createState() => _PokemonPageState();
}

class _PokemonPageState extends State<PokemonPage> {
  List _pokemons;
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Pokemon'),
        actions: [
          IconButton(
            icon: Icon(Icons.refresh),
            onPressed: _onRefreshPressed,
          )
        ],
      ),
      body: Center(
        child: _bodyDependingOnState(),
      ),
    );
  }

  _bodyDependingOnState() {
    if (_isLoading) {
      return CircularProgressIndicator();
    } else if (_pokemons == null) {
      return Text("Pas encore de pokemon !");
    } else {
      return new ListView.builder(
          itemCount: _pokemons.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return Container(
              margin: const EdgeInsets.all(10.0),
              color: Colors.amber[600],
              width: 400.0,
              height: 400.0,
              child: Column(
                children: <Widget>[
                  Text(_pokemons[index].name),
                  CachedNetworkImage(imageUrl: _pokemons[index].imageUrl),
                ],
              ),
            );
          });
    }
  }

  _onRefreshPressed() async {
    setState(() {
      _isLoading = true;
    });
    List receivedPokemons = await PokemonAPI.getPokemons();
    setState(() {
      _pokemons = receivedPokemons;
      _isLoading = false;
    });
  }
}
