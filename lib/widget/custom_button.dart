import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final String title;
  final Function onPressed;

  CustomButton(this.title, this.onPressed);

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  Color _backgroundColor;

  @override
  void initState() {
    super.initState();
    _backgroundColor = Colors.blueAccent;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: (detail) {
        setState(() {});
      },
      onTapUp: (detail) {
        setState(() {});
        widget.onPressed();
      },
      onTapCancel: () {
        setState(() {});
      },
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 48.0,
          decoration: BoxDecoration(
              color: _backgroundColor,
              borderRadius: BorderRadius.all(
                Radius.circular(14.0),
              ),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.6),
                  spreadRadius: 1.0,
                  blurRadius: 2.0,
                  offset: Offset(2.0, 2.0),
                ),
              ]),
          child: Center(
            child: Text(
              widget.title,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ),
      ),
    );
  }
}
