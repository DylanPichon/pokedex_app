import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class Pokemon {
  String imageUrl;
  String name;

  Pokemon(this.imageUrl, this.name);

  Pokemon.fromJSON(Map<String, dynamic> card) {
    this.imageUrl = card['imageUrl'];
    this.name = card['name'];
    downloadFile(this.imageUrl);
  }

  downloadFile(imageUrl) async {
    await DefaultCacheManager().downloadFile(imageUrl);
  }

  @override
  String toString() {
    return 'Pokemon{name:$name ,imageUrl: $imageUrl}';
  }
}
